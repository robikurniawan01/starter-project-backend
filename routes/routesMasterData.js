var express = require('express');
var MasterDataCtrl = require('../controllers/MasterData.controller')
var MasterAuthCtrl = require('../controllers/MasterAuth.controller')
var MasterRoleCtrl = require('../controllers/MasterRole.controller')
var auth = require('../tools/middleware');
var router = express.Router();


// Master Data Auth
router.get('/authorization', auth.authorization,  MasterAuthCtrl.getAuthorization)
router.get('/authorization/:id',  MasterAuthCtrl.getAuthorizationByID);
router.get('/authorization-by-site/:site', auth.authorization, MasterAuthCtrl.getAuthorizationBySite);
router.get('/authorization-by-code/:code', auth.authorization, MasterAuthCtrl.getAuthorizationByEmployeeCode);
router.get('/autocomplete-employee/:term', auth.authorization,  MasterAuthCtrl.getAutocompleteEmployee);
router.post('/authorization',  MasterAuthCtrl.addAuthorization)
router.put('/authorization/:id',  MasterAuthCtrl.updateAuthorization);
router.put('/delete-authorization/:id',  MasterAuthCtrl.deleteAuthorization);

// Master Data Role
router.get('/role', auth.authorization,  MasterRoleCtrl.getAllRole)


module.exports = router;