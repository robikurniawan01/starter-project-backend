var DataTypes = require("sequelize").DataTypes;
var _bypass_password = require("./bypass_password");
var _mst_authorization = require("./mst_authorization");
var _mst_role = require("./mst_role");

function initModels(sequelize) {
  var bypass_password = _bypass_password(sequelize, DataTypes);
  var mst_authorization = _mst_authorization(sequelize, DataTypes);
  var mst_role = _mst_role(sequelize, DataTypes);


  return {
    bypass_password,
    mst_authorization,
    mst_role,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
